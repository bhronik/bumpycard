package com.bumpycard;

import java.nio.charset.Charset;

import com.bumpycard.constants.NdefRecordsConstants;
import com.bumpycard.helpers.CardValidator;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link CardDataSendFragment.OnFragmentInteractionListener} interface to
 * handle interaction events. Use the {@link CardDataSendFragment#newInstance}
 * factory method to create an instance of this fragment.
 * 
 */
public class CardDataSendFragment extends Fragment implements CreateNdefMessageCallback {

	private final String TAG = getClass().getName();
	
	private TextView mNameLabelTv;
	private TextView mCompanyLabelTv;
	private TextView mEmailLabelTv;
	private TextView mCellPhoneLabelTv;
	private TextView mHomePhoneLabelTv;
	private TextView mWorkPhoneLabelTv;
	private TextView mNameContentTv;
	private TextView mCompanyContentTv;
	private TextView mEmailContentTv;
	private TextView mCellPhoneContentTv;
	private TextView mHomePhoneContentTv;
	private TextView mWorkPhoneContentTv;
	private TextView mErrorTv;
	
	private TextView mSendButton;
	
	private NfcAdapter mNfcAdapter;
	
	private SharedPreferences mSharedPreferences;
	
	public CardDataSendFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		
		View viewer = inflater.inflate(R.layout.fragment_card_data_send, container,
				false);
		
		initializeNFC();
		
		initializeSharedPreferences();
		initializeTextViews(viewer);
		initializeButtons(viewer);
		testCardValidity();
		
		return viewer;
	}

	/**
	 * @param viewer
	 */
	private void initializeButtons(View viewer) {
		mSendButton = (TextView) viewer.findViewById(R.id.fragment_card_data_send_button);
		mSendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	/**
	 * 
	 */
	private void testCardValidity() {
		// TODO Auto-generated method stub
		if (!CardValidator.isCardValid(mSharedPreferences.getString(getString(R.string.prefs_key_name), ""),
				mSharedPreferences.getString(getString(R.string.prefs_key_email), ""),
				mSharedPreferences.getString(getString(R.string.prefs_key_cell_phone), ""),
				mSharedPreferences.getString(getString(R.string.prefs_key_work_phone), ""),
				mSharedPreferences.getString(getString(R.string.prefs_key_home_phone), ""))) {
			mNameLabelTv.setVisibility(View.GONE);
			mNameContentTv.setVisibility(View.GONE);
			mCompanyLabelTv.setVisibility(View.GONE);
			mCompanyContentTv.setVisibility(View.GONE);
			mEmailLabelTv.setVisibility(View.GONE);
			mEmailContentTv.setVisibility(View.GONE);
			mCellPhoneLabelTv.setVisibility(View.GONE);
			mCellPhoneContentTv.setVisibility(View.GONE);
			mWorkPhoneLabelTv.setVisibility(View.GONE);
			mWorkPhoneContentTv.setVisibility(View.GONE);
			mHomePhoneLabelTv.setVisibility(View.GONE);
			mHomePhoneContentTv.setVisibility(View.GONE);
			mSendButton.setVisibility(View.GONE);
			mErrorTv.setVisibility(View.VISIBLE);
		} else {
			mNameLabelTv.setVisibility(View.VISIBLE);
			mNameContentTv.setVisibility(View.VISIBLE);
			mCompanyLabelTv.setVisibility(View.VISIBLE);
			mCompanyContentTv.setVisibility(View.VISIBLE);
			mEmailLabelTv.setVisibility(View.VISIBLE);
			mEmailContentTv.setVisibility(View.VISIBLE);
			mCellPhoneLabelTv.setVisibility(View.VISIBLE);
			mCellPhoneContentTv.setVisibility(View.VISIBLE);
			mWorkPhoneLabelTv.setVisibility(View.VISIBLE);
			mWorkPhoneContentTv.setVisibility(View.VISIBLE);
			mHomePhoneLabelTv.setVisibility(View.VISIBLE);
			mHomePhoneContentTv.setVisibility(View.VISIBLE);
			mSendButton.setVisibility(View.VISIBLE);
			mErrorTv.setVisibility(View.GONE);
		}

	}

	private void initializeSharedPreferences() {
		mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.bumpy_card_prefs), Context.MODE_PRIVATE);
	}
	
	/**
	 * @param viewer
	 */
	private void initializeTextViews(View viewer) {
		mNameLabelTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_name_label);
		mNameContentTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_name_content);
		mNameContentTv.setText(mSharedPreferences.getString(getString(R.string.prefs_key_name), ""));
		
		mCompanyLabelTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_company_label);
		mCompanyContentTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_company_content);
		mCompanyContentTv.setText(mSharedPreferences.getString(getString(R.string.prefs_key_company), ""));
		
		mEmailLabelTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_email_label);
		mEmailContentTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_email_content);
		mEmailContentTv.setText(mSharedPreferences.getString(getString(R.string.prefs_key_email), ""));
		
		mCellPhoneLabelTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_cell_phone_label);
		mCellPhoneContentTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_cell_phone_content);
		mCellPhoneContentTv.setText(mSharedPreferences.getString(getString(R.string.prefs_key_cell_phone), ""));
		
		mHomePhoneLabelTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_home_phone_label);
		mHomePhoneContentTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_home_phone_content);
		mHomePhoneContentTv.setText(mSharedPreferences.getString(getString(R.string.prefs_key_home_phone), ""));
				
		mWorkPhoneLabelTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_work_phone_label);
		mWorkPhoneContentTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_work_phone_content);
		mWorkPhoneContentTv.setText(mSharedPreferences.getString(getString(R.string.prefs_key_work_phone), ""));
		
		mErrorTv = (TextView) viewer.findViewById(R.id.fragment_card_data_send_error_message);
	}

	/**
	 * initilazed the nfc adapter, and exits if not availiable
	 */
	private void initializeNFC() {
		Activity parent = getActivity();
        mNfcAdapter = NfcAdapter.getDefaultAdapter(parent);
        if (mNfcAdapter == null) {
            Toast.makeText(parent, "NFC is not available", Toast.LENGTH_LONG).show();
            parent.finish();
            return;
        }
        mNfcAdapter.setNdefPushMessageCallback(this, parent);
	}
	
    /** CreateNDEFCallback methods **/
    
    /**
     * callback for creating an NDEFmessagge when in range
     * 
     */
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
    	Log.i(TAG, "createNdefMessage called");
    	
    	
    	NdefRecord[] records = createRecords();
        NdefMessage message = new NdefMessage(records);
        return message;
    }
    
    /**
     * helper methods to create the array of ndefrecords to serve as payload
     * 
     * @return array of NdefRecord corresponding to the payload to be sent
     */
    private NdefRecord[] createRecords() {
    	NdefRecord[] records = new NdefRecord[NdefRecordsConstants.MESSAGE_LENGTH];
    	
    	records[NdefRecordsConstants.NAME_INDEX] = new NdefRecord(
    		    NdefRecord.TNF_MIME_MEDIA ,
    		    getString(R.string.ndef_domain).getBytes(Charset.forName("US-ASCII")),
    		    new byte[0], mSharedPreferences.getString(getString(R.string.prefs_key_name), "").getBytes(Charset.forName("US-ASCII")));
    	
    	records[NdefRecordsConstants.COMPANY_INDEX] = new NdefRecord(
    		    NdefRecord.TNF_MIME_MEDIA ,
    		    getString(R.string.ndef_domain).getBytes(Charset.forName("US-ASCII")),
    		    new byte[0], mSharedPreferences.getString(getString(R.string.prefs_key_company), "").getBytes(Charset.forName("US-ASCII")));
    	
    	records[NdefRecordsConstants.EMAIL_INDEX] = new NdefRecord(
    		    NdefRecord.TNF_MIME_MEDIA ,
    		    getString(R.string.ndef_domain).getBytes(Charset.forName("US-ASCII")),
    		    new byte[0], mSharedPreferences.getString(getString(R.string.prefs_key_email), "").getBytes(Charset.forName("US-ASCII")));
    	
    	records[NdefRecordsConstants.CELL_PHONE_NUMBER_INDEX] = new NdefRecord(
    		    NdefRecord.TNF_MIME_MEDIA ,
    		    getString(R.string.ndef_domain).getBytes(Charset.forName("US-ASCII")),
    		    new byte[0], mSharedPreferences.getString(getString(R.string.prefs_key_cell_phone), "").getBytes(Charset.forName("US-ASCII")));
    	
    	records[NdefRecordsConstants.HOME_PHONE_NUMBER_INDEX] = new NdefRecord(
    		    NdefRecord.TNF_MIME_MEDIA ,
    		    getString(R.string.ndef_domain).getBytes(Charset.forName("US-ASCII")),
    		    new byte[0], mSharedPreferences.getString(getString(R.string.prefs_key_home_phone), "").getBytes(Charset.forName("US-ASCII")));
    	
    	records[NdefRecordsConstants.WORK_PHONE_NUMBER_INDEX] = new NdefRecord(
    		    NdefRecord.TNF_MIME_MEDIA ,
    		    getString(R.string.ndef_domain).getBytes(Charset.forName("US-ASCII")),
    		    new byte[0], mSharedPreferences.getString(getString(R.string.prefs_key_work_phone), "").getBytes(Charset.forName("US-ASCII")));
    	
    	records[NdefRecordsConstants.AAR_INDEX] = NdefRecord.createApplicationRecord(
    			getString(R.string.package_name));
    	
    	return records;
    }
}
