package com.bumpycard;

import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumpycard.entities.ContentWrapper;
import com.bumpycard.entities.Record;

/**
 * A fragment representing a single Card detail screen. This fragment is either
 * contained in a {@link CardListActivity} in two-pane mode (on tablets) or a
 * {@link CardDetailActivity} on handsets.
 */
public class CardDetailFragment extends Fragment {
	
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";
	
	/**
	 * Tag used in Logcat.
	 */
	public final String TAG = getClass().getName();
	
	/**
	 * The record to be displayed in the fragment
	 */
	private Record mRecord;
	
	/**
	 * TextView to display label of name.
	 */
	private TextView mNameLabelTv;
	/**
	 * The textview to display the name in the record.
	 */
	private TextView mNameTv;
	/**
	 * TextView to display the label of company.
	 */
	private TextView mCompanyLabelTv;
	/**
	 * The textview to display the company in the record. 
	 */
	private TextView mCompanyTv;
	/**
	 * TextView to display the lavel of email.
	 */
	private TextView mEmailLabelTv;
	/**
	 * The textview to display the email in the record.
	 */
	private TextView mEmailTv;
	/**
	 * TextView to display the label of cell phone.
	 */
	private TextView mCellPhoneLabelTv;
	/**
	 * The textview to display the cell phone in the record.
	 */
	private TextView mCellPhoneTv;
	/**
	 * TextView to display the label of home phone.
	 */
	private TextView mHomePhoneLabelTv;
	/**
	 * The textview to display the home phone in the record.
	 */
	private TextView mHomePhoneTv;
	/**
	 * TextView to display the label of work phone.
	 */
	private TextView mWorkPhoneLabelTv;
	/**
	 * The textview to display the work phone in the record.
	 */
	private TextView mWorkPhoneTv;

	/**
	 * ContentWrapper used to retrieve record from db.
	 */
	private ContentWrapper mContentWrapper;
	
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public CardDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.i(TAG, "onCreate");
		mContentWrapper = new ContentWrapper(getActivity().getContentResolver());

		Bundle args = getArguments();
		if (null != args && args.containsKey(ARG_ITEM_ID)) {
			int id = args.getInt(ARG_ITEM_ID);
			ArrayList<Integer> argList = new ArrayList<Integer>();
			argList.add(id);
			Record[] records = mContentWrapper.getRecordsGivenRecordIds(argList);
			if (null != records && records.length > 0) {
				mRecord = records[0];
			} else {
				//TODO handle error, but shouldn't happen
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		
		View rootView = inflater.inflate(R.layout.fragment_card_detail,
				container, false);

		// Show the dummy content as text in a TextView.
		initializeTextViews(rootView);

		return rootView;
	}
	
	/**
	 * Inflates the textviews from their corresponding views in the layout.
	 * 
	 * @param viewer	view which contains the textview.
	 */
	private void initializeTextViews(View viewer) {
		mNameLabelTv = (TextView) viewer.findViewById(R.id.card_detail_name_label_tv);
		mNameTv = (TextView) viewer.findViewById(R.id.card_detail_name_tv);
		
		mCompanyLabelTv = (TextView) viewer.findViewById(R.id.card_detail_company_label_tv);
		mCompanyTv = (TextView) viewer.findViewById(R.id.card_detail_company_tv);
		
		mEmailLabelTv = (TextView) viewer.findViewById(R.id.card_detail_email_label_tv);
		mEmailTv = (TextView) viewer.findViewById(R.id.card_detail_email_tv);
		mEmailTv.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (null != mRecord && null != mRecord.getEmail())
					startEmailActivity(mRecord.getEmail());
				return true;
			}
		});
		
		mCellPhoneLabelTv = (TextView) viewer.findViewById(R.id.card_detail_cell_phone_label_tv);
		mCellPhoneTv = (TextView) viewer.findViewById(R.id.card_detail_cell_phone_tv);
		mCellPhoneTv.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (null != mRecord && null != mRecord.getPh_cell())
					startPhoneCallActivity(mRecord.getPh_cell());
				return true;
			}
		});
		
		mHomePhoneLabelTv = (TextView) viewer.findViewById(R.id.card_detail_home_phone_label_tv);
		mHomePhoneTv = (TextView) viewer.findViewById(R.id.card_detail_home_phone_tv);
		mHomePhoneTv.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (null != mRecord && null != mRecord.getPh_home())
					startPhoneCallActivity(mRecord.getPh_home());
				return true;
			}
		});
		
		mWorkPhoneLabelTv = (TextView) viewer.findViewById(R.id.card_detail_work_phone_label_tv);
		mWorkPhoneTv = (TextView) viewer.findViewById(R.id.card_detail_work_phone_tv);
		mWorkPhoneTv.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (null != mRecord && null != mRecord.getPh_work())
					startPhoneCallActivity(mRecord.getPh_work());
				return true;
			}
		});
		
		setTextFromRecord();
	}
	
	/**
	 * Sets the text of the textviews from their corresponding member in the record.
	 */
	private void setTextFromRecord() {
		if (mRecord != null) {
			mNameTv.setText(mRecord.getName());
			mCompanyTv.setText(mRecord.getCompany());
			mEmailTv.setText(mRecord.getEmail());
			mCellPhoneTv.setText(mRecord.getPh_cell());
			mHomePhoneTv.setText(mRecord.getPh_home());
			mWorkPhoneTv.setText(mRecord.getPh_work());
			clearBlankFields();
		} else {
			clearName();
			clearCompany();
			clearEmail();
			clearCellPhone();
			clearHomePhone();
			clearWorkPhone();
		}
	}

	/**
	 * 
	 */
	private void clearWorkPhone() {
		mWorkPhoneLabelTv.setVisibility(View.GONE);
		mWorkPhoneTv.setVisibility(View.GONE);
	}

	/**
	 * 
	 */
	private void clearHomePhone() {
		mHomePhoneLabelTv.setVisibility(View.GONE);
		mHomePhoneTv.setVisibility(View.GONE);
	}

	/**
	 * 
	 */
	private void clearCellPhone() {
		mCellPhoneLabelTv.setVisibility(View.GONE);
		mCellPhoneTv.setVisibility(View.GONE);		
	}

	/**
	 * 
	 */
	private void clearEmail() {
		mEmailLabelTv.setVisibility(View.GONE);
		mEmailTv.setVisibility(View.GONE);		
	}

	/**
	 * 
	 */
	private void clearCompany() {
		mCompanyLabelTv.setVisibility(View.GONE);
		mCompanyTv.setVisibility(View.GONE);		
	}

	/**
	 * 
	 */
	private void clearName() {
		mNameLabelTv.setVisibility(View.GONE);
		mNameTv.setVisibility(View.GONE);		
	}

	/**
	 * precondition: mRecord is verified non null
	 */
	private void clearBlankFields() {
		if (mRecord.getName() == null || "".equals(mRecord.getName().trim()))
				clearName();
		if (mRecord.getCompany() == null || "".equals(mRecord.getCompany().trim()))
			clearCompany();
		if (mRecord.getEmail() == null || "".equals(mRecord.getEmail().trim()))
			clearEmail();
		if (mRecord.getPh_cell() == null || "".equals(mRecord.getPh_cell().trim()))
			clearCellPhone();
		if (mRecord.getPh_work() == null || "".equals(mRecord.getPh_work().trim()))
			clearWorkPhone();
		if (mRecord.getPh_home() == null || "".equals(mRecord.getPh_home().trim()))
			clearHomePhone();
	}
	
	private void startEmailActivity(String email) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
		intent.setType("text/plain");
		startActivity(Intent.createChooser(intent, "Send email"));
	}
	
	private void startPhoneCallActivity(String phoneNumber) {
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
		startActivity(Intent.createChooser(intent, "Phone Call"));
	}
}
