package com.bumpycard;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bumpycard.dummy.DummyDBTest;
import com.bumpycard.entities.Record;
import com.crittercism.app.Crittercism;

import com.newrelic.agent.android.NewRelic;

public class MainActivity extends ActionBarActivity implements
		CardListFragment.Callbacks {

	/**
	 * Tag used for Logcat
	 */
	private final String TAG = getClass().getName();
	
	/**
	 * List of Fragments to be displayed in the NavigationDrawer.
	 */
	private Fragment[] mFragments = {new CardDataCreateFragment(), new CardListFragment(), new CardDataSendFragment()};
	
	/**
	 * List of strings to be displayed in the NavigationDrawer.
	 */
	private String[] mDrawerItems = {"Edit your BumpyCard", "View BumpyCards", "Send your BumpyCard"};
	
	/**
	 * DrawerLayout to provide top level navigation.
	 */
	private DrawerLayout mDrawerLayout;
	
	/**
	 * ListView to display names of navigation options.
	 */
	private ListView mDrawerListView;
	
	/**
	 * ActionBar where NavigationDrawer lives.
	 */
	private ActionBar mActionBar;

	/**
	 * Responsible for implement opening and closing Callbacks for NavigationDrawer.
	 */
	private ActionBarDrawerToggle mActionBarDrawerToggle;
	
	/**
	 * callback which is called when Activity is created
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crittercism.init(getApplicationContext(), getString(R.string.crittercism_id));
		setContentView(R.layout.activity_main);
		
		Log.i(TAG, "onCreate");
		
		initializeNewRelic();
//		initializeFragments();
		initializeNavigationDrawer();
		//---------------------------------------------------------------------------
//		DummyDBTest test = new DummyDBTest(getContentResolver());
//		test.startTest();
		//---------------------------------------------------------------------------
		if (null == savedInstanceState)
			setFragment(0);
	}

	/**
	 * callback which is called when optionsMenu is created
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		Log.i(TAG, "onCreateOptionsMenu");
		return true;
	}

    /**
     * called when activity is restarted
     */
    @Override
    public void onRestart() {
        super.onRestart();

        Log.i(TAG, "onRestart");
    }
    
    /**
     * called when activity is starting
     */
    @Override
    public void onStart() {
        super.onStart();

        Log.i(TAG, "onStart");
    }
    
    /**
     * called when activity is stopped
     */
	@Override
	protected void onStop()
	{
		super.onStop();		

		Log.i(TAG, "onStop");
	}
    
    /**
     * called when activity resumes
     */
    @Override
    public void onResume() {
        super.onResume();
        
        Log.i(TAG, "onResume");
    }
    
    /**
     * called when activity is paused
     */
    @Override
    public void onPause() {
        super.onPause();
       
        Log.i(TAG, "onPause" + (isFinishing() ? " Finishing" : " Not Finishing"));
    }
    
    /**
     * called when activity is destroyed
     */
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	
    	Log.i(TAG, "onDestroy");
    	
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        } else if (android.R.id.home == item.getItemId()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                           .replace(R.id.main_temp_container, mFragments[1])
                           .commit();
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
    
    private void initializeNewRelic() {
    	NewRelic.withApplicationToken(
    			"AA2faad8e2003e3e155da076c921bc31ad4bf295ad"
    			).start(this.getApplication());
    }
    
	/**
	 * creates the fragments and adds them to the container
	 */
	private void initializeFragments() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		CardDataCreateFragment fragment = new CardDataCreateFragment();
		fragmentTransaction.add(R.id.main_temp_container, fragment);
		fragmentTransaction.commit();
	}
	
	/**
	 * initializes the NavigationDrawer
	 */
	private void initializeNavigationDrawer() {
		mActionBar = getSupportActionBar();
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
		
		mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_closed);
		
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        
        mDrawerListView = (ListView) findViewById(R.id.main_left_drawer);
        mDrawerListView.setAdapter(new ArrayAdapter<String>(this,
        		R.layout.drawer_list_item, mDrawerItems));
        mDrawerListView.setOnItemClickListener(new DrawerItemClickListener());
        
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);
	}
	
    /**
     * Sets the fragment to the one selected in the navigation drawer.
     *
     * @param position      the position of the fragment to be set
     */
    private void setFragment(int position) {
            if (position >= 0 && position < mFragments.length) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                               .replace(R.id.main_temp_container, mFragments[position])
                               .commit();
            }
    }
    
	/**
	 * Callback method from {@link CardListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(Record record) {
		//TODO possibly add twopane
		// In single-pane mode, simply start the detail activity
		// for the selected item ID.
		Log.i(TAG, "onItemSelected");
		if (record != null) {
			Bundle bundle = new Bundle();
			bundle.putInt(CardDetailFragment.ARG_ITEM_ID, record.getId());
			Fragment fragment = new CardDetailFragment();
			fragment.setArguments(bundle);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                           .replace(R.id.main_temp_container, fragment)
                           .commit();
            mActionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            //TODO change from nav drawer to up button
		}
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {

		/* (non-Javadoc)
		 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			setFragment(position);
		}
		
	}
}
