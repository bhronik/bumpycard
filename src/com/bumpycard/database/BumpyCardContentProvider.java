package com.bumpycard.database;


import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;


/*
 * This class implements the content provider. Houses the SQL methods.
 * 
 */
public class BumpyCardContentProvider extends ContentProvider{

	private static final String TAG = "BumpyCardContentProvider";
	
	//db name and version
	public static final String DB_NAME = "bumpy_card";
	public static final int DB_VERSION = 1;
	
	//table name and cols
	public static final String INTERMEDIATE_TABLE = "intermediate";
	public static final String INTERMEDIATE_TABLE_COL_1 = "_id";
	public static final String INTERMEDIATE_TABLE_COL_2 = "record_key";
	public static final String INTERMEDIATE_TABLE_COL_3 = "tag_key";
	public static final String[] INTERMEDIATE_TABLE_ALL_COLUMNS = {INTERMEDIATE_TABLE_COL_1,
		INTERMEDIATE_TABLE_COL_2,INTERMEDIATE_TABLE_COL_3};
	
	public static final String TAG_TABLE = "tag";
	public static final String TAG_TABLE_COL_1 = "_id";
	public static final String TAG_TABLE_COL_2 = "tag_name";
	public static final String[] TAG_TABLE_ALL_COLUMNS = {TAG_TABLE_COL_1,TAG_TABLE_COL_2};
	
	public static final String RECORDS_TABLE = "records";
	public static final String RECORDS_TABLE_COL_1 = "_id";
	public static final String RECORDS_TABLE_COL_2 = "name";
	public static final String RECORDS_TABLE_COL_3 = "phWork";
	public static final String RECORDS_TABLE_COL_4 = "phCell";
	public static final String RECORDS_TABLE_COL_5 = "phHome";
	public static final String RECORDS_TABLE_COL_6 = "email";
	public static final String RECORDS_TABLE_COL_7 = "website";
	public static final String RECORDS_TABLE_COL_8 = "address";
	public static final String RECORDS_TABLE_COL_9 = "company";
	public static final String RECORDS_TABLE_COL_10 = "position";
	public static final String RECORDS_TABLE_COL_11 = "type"; // used for internal classification
	public static final String[] RECORD_TABLE_ALL_COLUMNS = {
		RECORDS_TABLE_COL_1, RECORDS_TABLE_COL_2, RECORDS_TABLE_COL_3, RECORDS_TABLE_COL_4, RECORDS_TABLE_COL_5, RECORDS_TABLE_COL_6,
		RECORDS_TABLE_COL_7, RECORDS_TABLE_COL_8, RECORDS_TABLE_COL_9, RECORDS_TABLE_COL_10, RECORDS_TABLE_COL_11
	};
	
	private MainDatabaseHelper mainDBHelper;
	
	//uri matcher
	private static final UriMatcher uriMatcher;
	
	//authorities and content uri
	public static final String AUTHORITIES = "com.bumpycard.database.BumpyCardContentProvider";
	public static final Uri CONTENT_URI_RT  = Uri.parse("content://" + AUTHORITIES + "/" + RECORDS_TABLE);
	public static final Uri CONTENT_URI_IT  = Uri.parse("content://" + AUTHORITIES + "/" + INTERMEDIATE_TABLE);
	public static final Uri CONTENT_URI_TT  = Uri.parse("content://" + AUTHORITIES + "/" + TAG_TABLE);

	static{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITIES, INTERMEDIATE_TABLE, 1);
		uriMatcher.addURI(AUTHORITIES, TAG_TABLE, 2);
		uriMatcher.addURI(AUTHORITIES, RECORDS_TABLE, 3);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		Log.d(TAG, TAG + " delete");

		SQLiteDatabase db = mainDBHelper.getWritableDatabase();
		int count = 0;
		
		switch(uriMatcher.match(uri)){
		case 1:
			count = db.delete(INTERMEDIATE_TABLE, selection, selectionArgs);
			break;
		case 2:
			count = db.delete(TAG_TABLE, selection, selectionArgs);
			break;
		case 3:
			count = db.delete(RECORDS_TABLE, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri.toString());
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Long returnLong;
		Log.d(TAG," insert");
		SQLiteDatabase db = mainDBHelper.getWritableDatabase();
		switch(uriMatcher.match(uri)){
		case 1:
			returnLong = db.insert(INTERMEDIATE_TABLE, null, values);
			break;
		case 2:
			returnLong = db.insert(TAG_TABLE, null, values);
			break;
		case 3:
			returnLong =db.insert(RECORDS_TABLE, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri.toString());
		}
		return Uri.parse(returnLong.toString());
	}

	@Override
	public boolean onCreate() {
		Log.d(TAG," created :)");
		mainDBHelper = new MainDatabaseHelper(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Log.d(TAG," query");
		SQLiteDatabase db = mainDBHelper.getWritableDatabase();
		Cursor cursor = null;
		switch(uriMatcher.match(uri)){
		case 1:
			cursor = db.query(INTERMEDIATE_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
			break;
		case 2:
			cursor = db.query(TAG_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
			break;
		case 3:
			cursor = db.query(RECORDS_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri.toString());
		}
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		Log.d(TAG," update");
		SQLiteDatabase db = mainDBHelper.getWritableDatabase();
		int count = 0;
		
		switch(uriMatcher.match(uri)){
		case 1:
			count = db.update(INTERMEDIATE_TABLE, values, selection, selectionArgs);
			break;
		case 2:
			count = db.update(TAG_TABLE, values, selection, selectionArgs);
			break;
		case 3:
			count = db.update(RECORDS_TABLE, values, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri.toString());
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	
	private static final class MainDatabaseHelper extends SQLiteOpenHelper{

		private static final String SQL_CREATE_TABLE_RECORDS_TABLE = "CREATE TABLE "
				+ RECORDS_TABLE
				+ "("
				+ RECORDS_TABLE_COL_1
				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ RECORDS_TABLE_COL_2
				+ " TEXT, "  
				+ RECORDS_TABLE_COL_3
				+ " TEXT, "
				+ RECORDS_TABLE_COL_4
				+ " TEXT, "
				+ RECORDS_TABLE_COL_5
				+ " TEXT, " 
				+ RECORDS_TABLE_COL_6
				+ " TEXT, "
				+ RECORDS_TABLE_COL_7
				+ " TEXT, "
				+ RECORDS_TABLE_COL_8
				+ " TEXT, "
				+ RECORDS_TABLE_COL_9
				+ " TEXT, "
				+ RECORDS_TABLE_COL_10
				+ " TEXT, "
				+ RECORDS_TABLE_COL_11
				+ " LONG "
				+");";

		private static final String SQL_CREATE_TABLE_TAG_TABLE = "CREATE TABLE "
				+ TAG_TABLE
				+ " ("
				+ TAG_TABLE_COL_1
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ TAG_TABLE_COL_2
				+ " TEXT "
				+ " );";
		
		private static final String SQL_CREATE_TABLE_INTERMEDIATE_TABLE = "CREATE TABLE "
				+ INTERMEDIATE_TABLE
				+ " ("
				+ INTERMEDIATE_TABLE_COL_1
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ INTERMEDIATE_TABLE_COL_2
				+ " LONG, "
				+ INTERMEDIATE_TABLE_COL_3
				+ " LONG "
				+ ");";


		MainDatabaseHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
			Log.i(TAG,"MainDatabaseHelper");
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.i(TAG,"MainDatabaseHelper Oncreate");
			db.execSQL(SQL_CREATE_TABLE_RECORDS_TABLE);
			db.execSQL(SQL_CREATE_TABLE_TAG_TABLE);
			db.execSQL(SQL_CREATE_TABLE_INTERMEDIATE_TABLE);
		}

		private static final String SQL_DROP_QUERY = "DROP TABLE IF EXISTS "
				+ SQL_CREATE_TABLE_RECORDS_TABLE + "; " + "DROP TABLE IF EXISTS "
				+ SQL_CREATE_TABLE_TAG_TABLE + ";" + "DROP TABLE IF EXISTS "
						+ SQL_CREATE_TABLE_INTERMEDIATE_TABLE + "; ";

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.i(TAG,"MainDatabaseHelper Oncreate");
			db.execSQL(SQL_DROP_QUERY);
			onCreate(db);
		}
	}
}
