package com.bumpycard;

import com.bumpycard.constants.*;
import com.bumpycard.entities.ContentWrapper;
import com.bumpycard.entities.Record;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * NfcFoundActivity is the opened when a card is sent via nfc, displays the card information
 * and allows the user to save it.
 * 
 * @author brenthronk
 *
 */
public class NfcFoundActivity extends Activity {

	/**
	 * Tag used for logcat messages.
	 */
	private final String TAG = getClass().getName();
	
	/**
	 * Displays the name on the bumpycard.
	 */
	private TextView mNameTv;
	/**
	 * Displays the company on the bumpycard.
	 */
	private TextView mCompanyTv;
	/**
	 * Displays the email on the bumpycard.
	 */
	private TextView mEmailTv;
	/**
	 * Displays the cell phone number on the bumpycard.
	 */
	private TextView mCellPhoneNumberTv;
	/**
	 * Displays the home phone number on the bumpycard.
	 */
	private TextView mHomePhoneNumberTv;
	/**
	 * Displays the work phone number on the bumpycard.
	 */
	private TextView mWorkPhoneNumberTv;
	
	/**
	 * EditText allowing tags to be added to the received bumpycard.
	 */
	private EditText tagEt;
	
	/**
	 * Button to save the received bumpycard.
	 */
	private Button saveButton;
	
	/**
	 * Content resolver that monitors stored records.
	 */
	private ContentResolver mContentResolver;
	/**
	 * Content wrapper around stored records.
	 */
	private ContentWrapper mContentWrapper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nfc_found);
		
		Log.i(TAG, "onCreate");
		
		initializeContextWrapper();
		initializeTextViews();
		initializeEditText();
		initializeButton();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nfc_found, menu);
		
		Log.i(TAG, "onCreateOptionsMenu");
		
		return true;
	}

    @Override
    public void onRestart() {
        super.onRestart();

        Log.i(TAG, "onRestart");
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.i(TAG, "onStart");
    }
    
	@Override
	protected void onStop()
	{
		super.onStop();		

		Log.i(TAG, "onStop");
	}
    
    @Override
    public void onResume() {
        super.onResume();
        
        Log.i(TAG, "onResume");
        
        Intent intent = getIntent();
        if (intent == null) {
        	Log.d(TAG, "NFC intent null, onResume");
        	return;
        }
        
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
        	processIntent(getIntent());
        }
    }
    
    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }
    
    @Override
    public void onPause() {
        super.onPause();
       
        Log.i(TAG, "onPause" + (isFinishing() ? " Finishing" : " Not Finishing"));
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	
    	Log.i(TAG, "onDestroy");
    	
    }
    
    /**
     * Helper method to initialize content wrapper.
     */
    private void initializeContextWrapper() {
    	mContentResolver = getContentResolver();
    	mContentWrapper = new ContentWrapper(mContentResolver);
    }
    
    /**
     * Maps the textviews to their corresponding elements in layout.
     */
    private void initializeTextViews() {
    	mNameTv = (TextView) findViewById(R.id.nfc_found_name_filler);
    	mCompanyTv = (TextView) findViewById(R.id.nfc_found_company_filler);
    	mEmailTv = (TextView) findViewById(R.id.nfc_found_email_filler);
    	mCellPhoneNumberTv = (TextView) findViewById(R.id.nfc_found_cell_phone_number_filler);
    	mHomePhoneNumberTv = (TextView) findViewById(R.id.nfc_found_home_phone_number_filler);
    	mWorkPhoneNumberTv = (TextView) findViewById(R.id.nfc_found_work_phone_number_filler);
    	
    }
    
    /**
     * Maps the edittext to its corresponding element in layout.
     */
    private void initializeEditText() {
    	tagEt = (EditText) findViewById(R.id.nfc_found_tag_et);
    }
    
    /**
     * Maps the button to its corresponding element in layout.
     */
    private void initializeButton() {
    	saveButton = (Button) findViewById(R.id.nfc_found_save_button);
    	saveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				save();
			}
    		
    	});
    }
    
    /**
     * Parses the NDEF Message from the intent and prints to the TextViews.
     */
    void processIntent(Intent intent) {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsgs == null) {
        	Toast.makeText(this, "Sorry, but something went wrong during transmission" +
        			", please retry", Toast.LENGTH_LONG).show();
        	finish();
        } 
        // only one message sent during the beam
        NdefMessage message = (NdefMessage) rawMsgs[0];
        if (message == null || message.getRecords() == null) {
        	Toast.makeText(this, "Sorry, but something went wrong during transmission" +
        			", please retry", Toast.LENGTH_LONG).show();
        	finish();
        }
        
        NdefRecord[] records = message.getRecords();
        NdefRecord nameRecord = records[NdefRecordsConstants.NAME_INDEX];
        NdefRecord companyRecord = records[NdefRecordsConstants.COMPANY_INDEX];
        NdefRecord emailRecord = records[NdefRecordsConstants.EMAIL_INDEX];
        NdefRecord cellPhoneNumberRecord = records[NdefRecordsConstants.CELL_PHONE_NUMBER_INDEX];
        NdefRecord homePhoneNumberRecord = records[NdefRecordsConstants.HOME_PHONE_NUMBER_INDEX];
        NdefRecord workPhoneNumberRecord = records[NdefRecordsConstants.WORK_PHONE_NUMBER_INDEX];
        if (nameRecord == null || companyRecord == null || emailRecord == null ||
        		cellPhoneNumberRecord == null || homePhoneNumberRecord == null || workPhoneNumberRecord == null) {
        	Toast.makeText(this, "Sorry, but something went wrong during transmission" +
        			", please retry", Toast.LENGTH_LONG).show();
        	finish();
        }
        
        mNameTv.setText(new String(nameRecord.getPayload()));
        mCompanyTv.setText(new String(companyRecord.getPayload()));
        mEmailTv.setText(new String(emailRecord.getPayload()));
        mCellPhoneNumberTv.setText(new String(cellPhoneNumberRecord.getPayload()));
        mHomePhoneNumberTv.setText(new String(homePhoneNumberRecord.getPayload()));
        mWorkPhoneNumberTv.setText(new String(workPhoneNumberRecord.getPayload()));
    }
    
    /**
     * Saves recieved nfc and create pop up inquiring about want to save to contacts.
     */
    private void save() {
    	if ("".equals(tagEt.getText().toString().replaceAll("\\s+", ""))) {
    		Toast.makeText(this, "Sorry must add at least one tag", Toast.LENGTH_LONG).show();
    	} else {
    		Record record = new Record();
    		record.setName(mNameTv.getText().toString());
    		record.setCompany(mCompanyTv.getText().toString());
    		record.setEmail(mEmailTv.getText().toString());
    		record.setPh_cell(mCellPhoneNumberTv.getText().toString());
    		record.setPh_home(mHomePhoneNumberTv.getText().toString());
    		record.setPh_work(mWorkPhoneNumberTv.getText().toString());
    		record.setTags(tagEt.getText().toString().split(","));
    		mContentWrapper.addRecord(record);
    		Toast.makeText(this, getString(R.string.card_save_success), Toast.LENGTH_SHORT).show();
    		finish();
    	}
    }
}
