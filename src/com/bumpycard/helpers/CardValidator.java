/**
 * 
 */
package com.bumpycard.helpers;

/**
 * @author brenthronk
 *
 */
public class CardValidator {

	public static boolean isCardValid(String name, String email, String cellPhone,
			String homePhone, String workPhone) {
		if (!"".equals(name)) {
			return (!"".equals(email) || !"".equals(cellPhone) || !"".equals(homePhone) || !"".equals(workPhone));
		} else {
			return false;
		}
	}
}
