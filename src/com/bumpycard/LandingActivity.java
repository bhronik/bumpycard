package com.bumpycard;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class LandingActivity extends ActionBarActivity {

	private final String TAG = getClass().getName();
	
	private TextView mPromptTv;
	
	private FrameLayout mContainerFl;
	
	private Button mSignupButton;
	private Button mLinkedinButton;
	private Button mLoginButton;

	private LoginFragment mLoginFragment;
	private SignupFragment mSignupFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing);
		
		initializeTextView();
		initializeFrameLayout();
		initializeButtons();
		
		setLoginFragment();
		setLoginPrompt();
		hideLoginButton();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.landing, menu);
		return true;
	}

	/**
	 *
	 */
	private void initializeTextView() {
		mPromptTv = (TextView) findViewById(R.id.activity_landing_prompt_tv);
	}

	/**
	 * 
	 */
	private void initializeFrameLayout() {
		mContainerFl = (FrameLayout) findViewById(R.id.activity_landing_container_fl);
	}

	/**
	 * 
	 */
	private void initializeButtons() {
		mSignupButton = (Button) findViewById(R.id.activity_landing_signup_button);
		mSignupButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideSignupButton();
				setSignupFragment();
				setSignupPrompt();
			}
		});
		
		mLinkedinButton = (Button) findViewById(R.id.activity_landing_linkedin_button);
		mLinkedinButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideLinkedinButton();
				setLinkedinFragment();
				setLinkedinPrompt();
			}
		});
		
		mLoginButton = (Button) findViewById(R.id.activity_landing_login_button);
		mLoginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideLoginButton();
				setLoginFragment();
				setLoginPrompt();
			}
		});
	}

	private void setSignupPrompt() {
		mPromptTv.setText(R.string.activity_landing_prompt_tv_signup);
	}
	
	private void setLoginPrompt() {
		mPromptTv.setText(R.string.activity_landing_prompt_tv_login);
	}
	
	private void setLinkedinPrompt() {
		mPromptTv.setText(R.string.activity_landing_prompt_tv_Linkedin);
	}

	private void setSignupFragment() {
		if (null == mSignupFragment)
			mSignupFragment = new SignupFragment();
		setFragment(mSignupFragment);
	}
	
	private void setLoginFragment() {
		if (null == mLoginFragment)
			mLoginFragment = new LoginFragment();
		mLoginFragment.setmIsLinkedInLogin(false);
		mLoginFragment.setmIsNativeLogin(true);
		setFragment(mLoginFragment);
	}
	
	private void setLinkedinFragment() {
		if (null == mLoginFragment)
			mLoginFragment = new LoginFragment();
		mLoginFragment.setmIsLinkedInLogin(true);
		mLoginFragment.setmIsNativeLogin(false);
		setFragment(mLoginFragment);
	}	

	private void hideSignupButton() {
		mLinkedinButton.setVisibility(View.VISIBLE);
		mLoginButton.setVisibility(View.VISIBLE);
		mSignupButton.setVisibility(View.GONE);
	}
	
	private void hideLoginButton() {
		mLinkedinButton.setVisibility(View.VISIBLE);
		mLoginButton.setVisibility(View.GONE);
		mSignupButton.setVisibility(View.VISIBLE);
	}
	
	private void hideLinkedinButton() {
		mLinkedinButton.setVisibility(View.GONE);
		mLoginButton.setVisibility(View.VISIBLE);
		mSignupButton.setVisibility(View.VISIBLE);
	}
	
	private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                       .replace(R.id.activity_landing_container_fl, fragment)
                       .commit();
	}
}
