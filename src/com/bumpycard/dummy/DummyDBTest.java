package com.bumpycard.dummy;

import android.content.ContentResolver;
import android.util.Log;

import com.bumpycard.entities.ContentWrapper;
import com.bumpycard.entities.Record;

/*
 * This class is used to test the functionality of the content provider(BumpyCardContentProvider)
 * 
 * 
 */
public class DummyDBTest {
	ContentResolver contentResolver;
	Record record1;
	Record record2;
	
	private String TAG = getClass().getName();
	
	public DummyDBTest(ContentResolver contentResolver){
		this.contentResolver = contentResolver;
		
		String[] tags1 = {"engineer","lifter","coder"};
		
		record1 = new Record("Brent", "Qualcomm", "AwesomeMan",
				"1234", "4567", "7890", "www.fitsby.com",
				"brent@awesomeman.com","Boulder",tags1);
		
		String[] tags2 = {"engineer","gamer","coder"};
		
		record2 = new Record("Ashwin", "Qualcomm", "EpicGuy",
				"1234", "4567", "7890", "www.ashwn.com",
				"ashwin@epicguy.com","Boulder",tags2);		

	}
	
	public void startTest(){
		Log.i(TAG,"startTest");
		
		ContentWrapper contentWrapper = new ContentWrapper(contentResolver);
		contentWrapper.addRecord(record1);
		contentWrapper.addRecord(record2);
		contentWrapper.addRecord(record1);
		contentWrapper.addRecord(record2);
		contentWrapper.queryRecord();
		
		
		String[] tagNames = contentWrapper.getAllTags();
		
		for (String s : tagNames) {
			System.out.println("ash="+s);
		}
		
		System.out.println("engineer="+contentWrapper.getTagCount("engineer"));
		System.out.println("lifter="+contentWrapper.getTagCount("lifter"));

		Record[] records = contentWrapper.getRecordsForTag("lifter");
		
		for(int i = 0 ; i <records.length ;i++){
			System.out.println("final="+records[i].toString());
		}
	}
}
