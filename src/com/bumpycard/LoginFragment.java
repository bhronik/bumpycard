package com.bumpycard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class LoginFragment extends Fragment {

	private String TAG = getClass().getName();
	
	private EditText mEmailEt;
	private EditText mPasswordEt;
	private Button mSubmitButton;
	
	private boolean mIsNativeLogin;
	private boolean mIsLinkedInLogin;
	
	public LoginFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View viewer = inflater.inflate(R.layout.fragment_login, container, false);
		
		initializeEditTexts(viewer);
		initializeButtons(viewer);
		return viewer;
	}

	/**
	 * @param viewer
	 */
	private void initializeButtons(View viewer) {
		mSubmitButton = (Button) viewer.findViewById(R.id.fragment_login_submit_button);
		mSubmitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submit();
			}
		});
	}

	/**
	 * @param viewer
	 */
	private void initializeEditTexts(View viewer) {
		mEmailEt = (EditText) viewer.findViewById(R.id.fragment_login_email_et);
		mPasswordEt = (EditText) viewer.findViewById(R.id.fragment_login_password_et);
	}

	/**
	 * Validates the edittext fields and then submits to server.
	 */
	private void submit() {
		// TODO Auto-generated method stub
		String email = mEmailEt.getText().toString();
		String password = mPasswordEt.getText().toString();
		
		if (email == null || "".equals(email.trim()) || password == null || "".equals(password.trim()))  {
			Toast.makeText(getActivity(), getString(R.string.empty_email_or_password), Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (mIsNativeLogin) {
			submitNativeLogin(email, password);
		} else if (mIsLinkedInLogin) {
			submitLinkedInLogin(email, password);
		} else {
			//TODO handle error case
		}
	}
	
	/**
	 * Performs query for native login.
	 * 
	 * @param email		email of user, assumed non-empty, and non-null
	 * @param password	password of user, assumed non-empty, and non-null
	 */
	private void submitNativeLogin(String email, String password) {
		//TODO fill in
	}
	
	/**
	 * Performs query of linked in user.
	 * 
	 * @param email		email of user, assumed non-empty, and non-null
	 * @param password	password of user, assumed non-empty, and non-null
	 */
	private void submitLinkedInLogin(String email, String password) {
		//TODO fill in
	}
	
	/**
	 * Sets mIsNativeLogin.
	 * 
	 * @param mIsNativeLogin
	 */
	public void setmIsNativeLogin(boolean mIsNativeLogin) {
		this.mIsNativeLogin = mIsNativeLogin;
	}

	/**
	 * Sets mIsLinkedInLogin.
	 * 
	 * @param mIsLinkedInLogin
	 */
	public void setmIsLinkedInLogin(boolean mIsLinkedInLogin) {
		this.mIsLinkedInLogin = mIsLinkedInLogin;
	}
}
