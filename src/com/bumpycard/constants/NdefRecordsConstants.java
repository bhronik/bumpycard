package com.bumpycard.constants;

public class NdefRecordsConstants {

	public final static int NAME_INDEX = 0;
	public final static int COMPANY_INDEX = 1;
	public final static int EMAIL_INDEX = 2;
	public final static int CELL_PHONE_NUMBER_INDEX = 3;
	public final static int HOME_PHONE_NUMBER_INDEX = 4;
	public final static int WORK_PHONE_NUMBER_INDEX = 5;
	public final static int AAR_INDEX = 6;
	//TODO make array creation more elegant, less maintenance
	public final static int MESSAGE_LENGTH = 7;
}
