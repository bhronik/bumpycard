package com.bumpycard.entities;

import android.database.Cursor;

public class Record {

	private int id;
	private String name;
	private String company;
	private String position;
	private String ph_work;
	private String ph_cell;
	private String ph_home;
	private String website;
	private String email;
	private String address;
	private String type;
	private String[] tags;

	public Record(){
		this.name = "";
		this.company = "";
		this.position = "";
		this.ph_work = "";
		this.ph_cell = "";
		this.ph_home = "";
		this.website = "";
		this.email = "";
		this.address = "";
		this.tags = null;
	}
	
	public Record(String name, String company, String position,
			String ph_work, String ph_cell, String ph_home, String website,
			String email, String address, String[] tags) {
		this.name = name;
		this.company = company;
		this.position = position;
		this.ph_work = ph_work;
		this.ph_cell = ph_cell;
		this.ph_home = ph_home;
		this.website = website;
		this.email = email;
		this.address = address;
		this.tags = tags;
	}
	
	public static Record cursorToRecord(Cursor cursor){
		Record record = new Record();
		record.setId(cursor.getInt(0));
		record.setName(cursor.getString(1));
		record.setCompany(cursor.getString(8));
		record.setPosition(cursor.getString(9));
		record.setPh_work(cursor.getString(2));
		record.setPh_cell(cursor.getString(3));
		record.setPh_home(cursor.getString(4));
		record.setWebsite(cursor.getString(6));
		record.setEmail(cursor.getString(5));
		record.setAddress(cursor.getString(7));
		record.setType(cursor.getString(10));
		return record;
	}
	
	public String toString(){
		return this.getId() + " " + this.getName() + " " + this.getCompany() ;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPh_work() {
		return ph_work;
	}

	public void setPh_work(String ph_work) {
		this.ph_work = ph_work;
	}

	public String getPh_cell() {
		return ph_cell;
	}

	public void setPh_cell(String ph_cell) {
		this.ph_cell = ph_cell;
	}

	public String getPh_home() {
		return ph_home;
	}

	public void setPh_home(String ph_home) {
		this.ph_home = ph_home;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
