package com.bumpycard.entities;

import android.database.Cursor;

public class IntermediateRecord {
	private int record_id;
	private int tag_id;

	public IntermediateRecord(int record_id, int tag_id) {
		this.record_id = record_id;
		this.tag_id = tag_id;
	}

	public int getRecord_id() {
		return record_id;
	}

	public void setRecord_id(int record_id) {
		this.record_id = record_id;
	}

	public int getTag_id() {
		return tag_id;
	}

	public void setTag_id(int tag_id) {
		this.tag_id = tag_id;
	}

	public String toString() {
		return this.record_id + " " + tag_id;
	}

	public static IntermediateRecord cursorToIntermediateRecord(Cursor cursor) {
		return new IntermediateRecord(cursor.getInt(1), cursor.getInt(2));
	}
}
