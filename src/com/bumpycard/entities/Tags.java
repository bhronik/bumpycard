package com.bumpycard.entities;

import android.database.Cursor;

public class Tags {

	private int id;
	
	private String name;
	
	public Tags(){
		this.id = 0;
		this.name = "";
	}
	
	public Tags( String name){
		this.id =0;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String toString(){
		return this.id + " " + this.name;
	}
	
	public static Tags cursorToTags(Cursor cursor){
		Tags tags = new Tags();
		tags.setId(cursor.getInt(0));
		tags.setName(cursor.getString(1));
		return tags;
	}
}
