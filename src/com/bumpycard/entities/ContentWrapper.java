package com.bumpycard.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.bumpycard.database.BumpyCardContentProvider;

/*
 * This class talks with the content provider to add or get the information
 * from the internal database.
 * 
 */
public class ContentWrapper {
	private static String TAG = "ContentWrapper";
	ContentResolver contentResolver;

	public ContentWrapper(ContentResolver contentResolver) {
		this.contentResolver = contentResolver;
	}

	/**
	 * Update the existing passed in record.
	 * 
	 * @param record
	 *            the record to be updated
	 * @return true if successful, false otherwise
	 */
	public boolean updateRecord(Record record) {
		// TODO implemented by Ashwin
		Log.i(TAG, "updateRecord");

		return false;
	}

	/**
	 * Adds a record to the stored records.
	 * 
	 * @param record
	 *            the record to be added
	 * @return true if successful, false otherwise
	 */
	public int addRecord(Record record) {
		Log.i(TAG, "addRecord");
		ContentValues values = new ContentValues();
		int tagRecordId;
		int intermediateRecordId;
		int RecordRowId;

		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_2,
				record.getName());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_3,
				record.getPh_cell());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_4,
				record.getPh_work());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_5,
				record.getPh_home());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_6,
				record.getEmail());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_7,
				record.getWebsite());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_8,
				record.getAddress());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_9,
				record.getCompany());
		values.put(BumpyCardContentProvider.RECORDS_TABLE_COL_10,
				record.getPosition());

		RecordRowId = Integer.parseInt(contentResolver.insert(
				BumpyCardContentProvider.CONTENT_URI_RT, values).toString());

		Log.i(TAG, "Added a record row id - " + RecordRowId + " ");

		for (String tagName : record.getTags()) {
			tagRecordId = getTagId(tagName);
			if (tagRecordId == 0) {
				// the tag name is not present so add it to tag table
				tagRecordId = Integer.parseInt(addTag(new Tags(tagName))
						.toString());
				Log.i(TAG, "Tag record addded - " + tagRecordId);
			}

			// tag name is present so, add it to intermediate table
			intermediateRecordId = Integer.parseInt(addIntermediateRow(
					new IntermediateRecord(RecordRowId, tagRecordId))
					.toString());
			Log.i(TAG, "intermediate record addded - " + intermediateRecordId);
		}
		return RecordRowId;
	}

	/*
	 * adds a tag record in the tag table
	 */
	private Uri addTag(Tags tag) {
		Log.i(TAG, "addTag");

		ContentValues values = new ContentValues();
		values.put(BumpyCardContentProvider.TAG_TABLE_COL_2, tag.getName());

		return contentResolver.insert(BumpyCardContentProvider.CONTENT_URI_TT,
				values);
	}

	/*
	 * Add a record in the intermediate table
	 */
	private Uri addIntermediateRow(IntermediateRecord intermediateRecord) {
		Log.i(TAG, "addIntermediateRecord");

		ContentValues values = new ContentValues();
		values.put(BumpyCardContentProvider.INTERMEDIATE_TABLE_COL_2,
				intermediateRecord.getRecord_id());
		values.put(BumpyCardContentProvider.INTERMEDIATE_TABLE_COL_3,
				intermediateRecord.getTag_id());

		return contentResolver.insert(BumpyCardContentProvider.CONTENT_URI_IT,
				values);
	}

	/*
	 * Returns id if a tag is present in tag table else returns 0
	 */
	private int getTagId(String tagName) {

		String[] mSelectionArgs = { "" };
		String mSelectionClause;

		// If the word is the empty string, gets everything
		if (TextUtils.isEmpty(tagName)) {
			// Setting the selection clause to null will return all words
			mSelectionClause = null;
			mSelectionArgs[0] = "";

		} else {
			// Constructs a selection clause that matches the word that the user
			// entered.
			mSelectionClause = BumpyCardContentProvider.TAG_TABLE_COL_2
					+ " = ?";

			// Moves the user's input string to the selection arguments.
			mSelectionArgs[0] = tagName;

		}

		Cursor cursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_TT,
				BumpyCardContentProvider.TAG_TABLE_ALL_COLUMNS,
				mSelectionClause, mSelectionArgs, null);
		cursor.moveToFirst();

		Log.d(TAG, "cursor get count -" + cursor.getCount());
		if (cursor.getCount() == 0) {
			return 0;
		}

		return Tags.cursorToTags(cursor).getId();
	}

	/*
	 * A test mthod used for testing. Have to delete it later
	 */
	public boolean queryRecord() {
		Log.i(TAG, "queryRecord");

		Log.i(TAG,
				"------------------------contents of record table-------------------------");
		Cursor cursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_RT,
				BumpyCardContentProvider.RECORD_TABLE_ALL_COLUMNS, null, null,
				null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			Record record = Record.cursorToRecord(cursor);
			Log.i(TAG, record.toString());
			cursor.moveToNext();
		}

		Log.i(TAG,
				"------------------------contents of tags table-------------------------");

		cursor = contentResolver.query(BumpyCardContentProvider.CONTENT_URI_TT,
				BumpyCardContentProvider.TAG_TABLE_ALL_COLUMNS, null, null,
				null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			Tags tag = Tags.cursorToTags(cursor);
			Log.i(TAG, tag.toString());
			cursor.moveToNext();
		}

		Log.i(TAG,
				"------------------------contents of intermediate table-------------------------");

		cursor = contentResolver.query(BumpyCardContentProvider.CONTENT_URI_IT,
				BumpyCardContentProvider.INTERMEDIATE_TABLE_ALL_COLUMNS, null,
				null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			IntermediateRecord intRec = IntermediateRecord
					.cursorToIntermediateRecord(cursor);
			Log.i(TAG, intRec.toString());
			cursor.moveToNext();
		}

		String[] mSelectionArgs = { "" };

		String mSearchString = "Brent";
		String mSelectionClause;

		// If the word is the empty string, gets everything
		if (TextUtils.isEmpty(mSearchString)) {
			// Setting the selection clause to null will return all words
			mSelectionClause = null;
			mSelectionArgs[0] = "";

		} else {
			// Constructs a selection clause that matches the word that the user
			// entered.
			mSelectionClause = BumpyCardContentProvider.RECORDS_TABLE_COL_2
					+ " = ?";

			// Moves the user's input string to the selection arguments.
			mSelectionArgs[0] = mSearchString;

		}
		cursor = contentResolver.query(BumpyCardContentProvider.CONTENT_URI_RT,
				BumpyCardContentProvider.RECORD_TABLE_ALL_COLUMNS,
				mSelectionClause, mSelectionArgs, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			Record record = Record.cursorToRecord(cursor);
			Log.i(TAG, record.toString());
			cursor.moveToNext();
		}

		return false;
	}

	/**
	 * Obtain all of the records. INCOMPLETE
	 * 
	 * @return a map from tags to all records with given tag
	 */
	public Map<String, Record[]> getAllRecords() {
		// TODO must complete

		Log.i(TAG, "getAllRecords");
		Map<String, Record[]> map = new HashMap<String, Record[]>();

		/*
		 * Steps: 1. Go to tags table and get all the tags 2. For each tag get
		 * the record ids from the intermediate table 3. Get each record id and
		 * get the record and add it to an array 4. Finally add the tag name and
		 * the record array to the map
		 */

		// get the contents of tag table
		Cursor tagTableCursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_TT,
				BumpyCardContentProvider.TAG_TABLE_ALL_COLUMNS, null, null,
				null);
		tagTableCursor.moveToFirst();

		while (!tagTableCursor.isAfterLast()) {
			Tags tag = Tags.cursorToTags(tagTableCursor);
			Log.i(TAG, tag.toString());

			String iSelectionClause = BumpyCardContentProvider.INTERMEDIATE_TABLE_COL_3
					+ " = ? ";
			String[] iSelectionArgs = { tag.getId() + "" };
			Cursor intermediateTableCursor = contentResolver.query(
					BumpyCardContentProvider.CONTENT_URI_IT,
					BumpyCardContentProvider.INTERMEDIATE_TABLE_ALL_COLUMNS,
					iSelectionClause, iSelectionArgs, null);
			intermediateTableCursor.moveToFirst();

			while (!intermediateTableCursor.isAfterLast()) {
				IntermediateRecord intRec = IntermediateRecord
						.cursorToIntermediateRecord(intermediateTableCursor);
				Log.i(TAG, intRec.toString());

				String rSelectionClause = BumpyCardContentProvider.RECORDS_TABLE_COL_1
						+ " = ? ";
				String[] rSelectionArgs = { intRec.getRecord_id() + "" };
				Cursor recordTableCursor = contentResolver.query(
						BumpyCardContentProvider.CONTENT_URI_RT,
						BumpyCardContentProvider.RECORD_TABLE_ALL_COLUMNS,
						rSelectionClause, rSelectionArgs, null);
				recordTableCursor.moveToFirst();

				ArrayList<Record> arrayRecords = new ArrayList<Record>();

				while (!recordTableCursor.isAfterLast()) {
					Record record = Record.cursorToRecord(recordTableCursor);
					arrayRecords.add(record);

					recordTableCursor.moveToNext();
				}
				// map.put(intRec.get, value)
				intermediateTableCursor.moveToNext();
			}
			tagTableCursor.moveToNext();
		}

		return map;
	}

	/**
	 * Gets all of the possible tags.
	 * 
	 * @return an array of all of the tags
	 */
	public String[] getAllTags() {
		// get the contents of tag table
		Cursor tagTableCursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_TT,
				BumpyCardContentProvider.TAG_TABLE_ALL_COLUMNS, null, null,
				null);
		tagTableCursor.moveToFirst();
		ArrayList<String> tagNames = new ArrayList<String>();

		while (!tagTableCursor.isAfterLast()) {
			Tags tag = Tags.cursorToTags(tagTableCursor);
			tagNames.add(tag.getName());
			tagTableCursor.moveToNext();
		}
		return tagNames.toArray(new String[tagNames.size()]);
	}

	/**
	 * Get the number of records with the given tag.
	 * 
	 * @param tag
	 *            the tag whose count should be queried
	 * @return the count of the number of records with given tag
	 */
	public int getTagCount(String tag) {
		Log.i(TAG, "getTagCount");

		return getTagCount(getTagId(tag));
	}

	/*
	 * Returns the count of all the tags given a tag id
	 * 
	 * @param tagid
	 * 
	 * @return a count of the occurrences of the tag id
	 */
	private int getTagCount(int tagId) {

		String[] mSelectionArgs = { "" };

		String mSearchString = "" + tagId;
		int count = 0;

		String mSelectionClause;

		// If the word is the empty string, gets everything
		if (TextUtils.isEmpty(mSearchString)) {
			return 0;
		} else {
			// Constructs a selection clause that matches the word that the user
			// entered.
			mSelectionClause = BumpyCardContentProvider.INTERMEDIATE_TABLE_COL_3
					+ " = ?";

			// Moves the user's input string to the selection arguments.
			mSelectionArgs[0] = mSearchString;

		}

		Cursor cursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_IT,
				BumpyCardContentProvider.INTERMEDIATE_TABLE_ALL_COLUMNS,
				mSelectionClause, mSelectionArgs, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			count++;
			cursor.moveToNext();
		}

		return count;
	}

	/**
	 * Returns all the records with the given tag.
	 * 
	 * @param tag
	 *            the tags whose record will be queried against
	 * @return an array of records with the given tag
	 */
	public Record[] getRecordsForTag(String tag) {
		// TODO implement

		Log.i(TAG, "getRecordsForTag");

		// get tag id for the given tag name
		int tagId = getTagId(tag);

		// search for all the record ids which have the above tag id
		ArrayList<Integer> recordIds = getRecordIds(tagId);

		// get all the record for given record ids
		return getRecordsGivenRecordIds(recordIds);

	}

	/*
	 * getRecordsGivenRecordIds
	 * 
	 * @param Takes a array of record ids
	 * 
	 * @return returns array of records from the database
	 */
	public Record[] getRecordsGivenRecordIds(ArrayList<Integer> recordIds) {
		// TODO Auto-generated method stub

		if (recordIds.size() == 0) {
			return null;
		}

		Record[] records = new Record[recordIds.size()];
		String[] mSelectionArgs = new String[recordIds.size()];

		// calculate the number of Questions marks needed to add in selection
		// clause
		String numQMarks = "";

		for (int i = 0; i < recordIds.size(); i++) {
			mSelectionArgs[i] = recordIds.get(i).toString();
			if (i + 1 == recordIds.size()) {
				numQMarks = numQMarks + "?";
			} else {
				numQMarks = numQMarks + "?,";
			}
		}

		String mSelectionClause = BumpyCardContentProvider.RECORDS_TABLE_COL_1
				+ " IN(" + numQMarks + ")";

		System.out.println("---------------1--------------------");

		Cursor recordTableCursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_RT,
				BumpyCardContentProvider.RECORD_TABLE_ALL_COLUMNS,
				mSelectionClause, mSelectionArgs, null);
		recordTableCursor.moveToFirst();

		int i = 0;
		System.out.println("---------------2--------------------");
		while (!recordTableCursor.isAfterLast()) {
			Record record = Record.cursorToRecord(recordTableCursor);
			records[i++] = record;
			recordTableCursor.moveToNext();
		}

		System.out.println("---------------return--------------------");
		return records;
	}

	private ArrayList<Integer> getRecordIds(int tagId) {
		// TODO Auto-generated method stub
		String[] mSelectionArgs = { "" };

		String mSearchString = "" + tagId;

		String mSelectionClause;

		ArrayList<Integer> recordIds = new ArrayList<Integer>();

		// If the word is the empty string, gets everything
		if (TextUtils.isEmpty(mSearchString)) {
			return null;
		} else {
			// Constructs a selection clause that matches the word that the user
			// entered.
			mSelectionClause = BumpyCardContentProvider.INTERMEDIATE_TABLE_COL_3
					+ " = ?";

			// Moves the user's input string to the selection arguments.
			mSelectionArgs[0] = mSearchString;
		}

		Cursor cursor = contentResolver.query(
				BumpyCardContentProvider.CONTENT_URI_IT,
				BumpyCardContentProvider.INTERMEDIATE_TABLE_ALL_COLUMNS,
				mSelectionClause, mSelectionArgs, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			IntermediateRecord intRec = IntermediateRecord
					.cursorToIntermediateRecord(cursor);
			recordIds.add(intRec.getRecord_id());
			cursor.moveToNext();
		}

		return recordIds;
	}

}
