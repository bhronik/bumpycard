package com.bumpycard;

import java.nio.charset.Charset;

import com.bumpycard.constants.NdefRecordsConstants;
import com.bumpycard.entities.ContentWrapper;
import com.bumpycard.entities.Record;
import com.bumpycard.helpers.CardValidator;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CardDataCreateFragment extends Fragment {

	/**
	 * Tag used for Logcat messages.
	 */
	private final String TAG = getClass().getName();
	
	private EditText mNameEt;
	private EditText mCompanyEt;
	private EditText mEmailEt;
	private EditText mCellPhoneNumberEt;
	private EditText mHomePhoneNumberEt;
	private EditText mWorkPhoneNumberEt;
	
	private Button mSaveButton;
	
	private ContentResolver mContentResolver;
	private ContentWrapper mContentWrapper;
	private SharedPreferences mSharedPreferences;
	
	/**
	 * callback for the creation of the fragment
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	    Log.i(TAG, "onCreateView");
	    
	    View viewer = (View) inflater.inflate(R.layout.card_data_create_fragment, container, false);
	    
	    initializeSharedPreferences();
	    initializeEditTexts(viewer);
	    initializeButtons(viewer);
	    initializeContentWrapper();
	    
	    return viewer;
	}
	
	/**
	 * callback for when this fragment is attached to a view
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.i(TAG, "onAttach");

	}
	
	/**
	 * callback for when the fragment is resumed
	 */
	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, "onResume");
	}
	
	private void initializeSharedPreferences() {
		mSharedPreferences = getActivity().getSharedPreferences(getString(R.string.bumpy_card_prefs), Context.MODE_PRIVATE);
	}
	
	/**
	 * initializes the EditTexts to their corresponding in viewer
	 * 
	 * @param viewer view where EditTexts reside
	 */
	private void initializeEditTexts(View viewer) {
		mNameEt = (EditText) viewer.findViewById(R.id.card_data_create_fragment_name_et);
		mCompanyEt = (EditText) viewer.findViewById(R.id.card_data_create_fragment_company_et);
		mEmailEt = (EditText) viewer.findViewById(R.id.card_data_create_fragment_email_et);
		mCellPhoneNumberEt = (EditText) viewer.findViewById(R.id.card_data_create_fragment_cell_phone_number_et);
		mHomePhoneNumberEt = (EditText) viewer.findViewById(R.id.card_data_create_fragment_home_phone_number_et);
		mWorkPhoneNumberEt = (EditText) viewer.findViewById(R.id.card_data_create_fragment_work_number_et);
		
		populateEditTextsFromPreferences();
	}
	
	private void populateEditTextsFromPreferences() {
		mNameEt.setText(mSharedPreferences.getString(getString(R.string.prefs_key_name), ""));
		mCompanyEt.setText(mSharedPreferences.getString(getString(R.string.prefs_key_company), ""));
		mEmailEt.setText(mSharedPreferences.getString(getString(R.string.prefs_key_email), ""));
		mCellPhoneNumberEt.setText(mSharedPreferences.getString(getString(R.string.prefs_key_cell_phone), ""));		
		mHomePhoneNumberEt.setText(mSharedPreferences.getString(getString(R.string.prefs_key_home_phone), ""));
		mWorkPhoneNumberEt.setText(mSharedPreferences.getString(getString(R.string.prefs_key_work_phone), ""));
	}
	private void initializeButtons(View viewer) {
		mSaveButton = (Button) viewer.findViewById(R.id.card_data_create_fragment_save_button);
		mSaveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				save();
			}
			
		});
	}
	
	private void save() {
		//saveRecord();
		saveMe();
	}
	
	private void saveRecord() {
		Record record = new Record();
		record.setName(mNameEt.getText().toString());
		record.setCompany(mCompanyEt.getText().toString());
		record.setEmail(mEmailEt.getText().toString());
		record.setPh_cell(mCellPhoneNumberEt.getText().toString());
		record.setPh_home(mHomePhoneNumberEt.getText().toString());
		record.setPh_work(mWorkPhoneNumberEt.getText().toString());
		mContentWrapper.addRecord(record);
	}
	
	private void saveMe() {
		if (CardValidator.isCardValid(mNameEt.getText().toString(),
				mEmailEt.getText().toString(), mCellPhoneNumberEt.getText().toString(),
				mHomePhoneNumberEt.getText().toString(), mWorkPhoneNumberEt.getText().toString())) {
			SharedPreferences.Editor editor = mSharedPreferences.edit();
			editor.putString(getString(R.string.prefs_key_name), mNameEt.getText().toString());
			editor.putString(getString(R.string.prefs_key_company), mCompanyEt.getText().toString());
			editor.putString(getString(R.string.prefs_key_email), mEmailEt.getText().toString());
			editor.putString(getString(R.string.prefs_key_cell_phone), mCellPhoneNumberEt.getText().toString());
			editor.putString(getString(R.string.prefs_key_work_phone), mWorkPhoneNumberEt.getText().toString());
			editor.putString(getString(R.string.prefs_key_home_phone), mHomePhoneNumberEt.getText().toString());
			editor.commit();
			
			Toast.makeText(getActivity(), "Updated information!", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(getActivity(), getString(R.string.name_phone_email_requirement_message), Toast.LENGTH_SHORT).show();
		}
	}
	
	private void initializeContentWrapper() {
		mContentResolver = getActivity().getContentResolver();
		mContentWrapper = new ContentWrapper(mContentResolver);
	}

}
