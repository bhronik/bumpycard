package com.bumpycard;

import android.app.Activity;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class CardDataReceiveFragment extends Fragment {

	private NfcAdapter mNfcAdapter;
	
	public CardDataReceiveFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View viewer = inflater.inflate(R.layout.fragment_card_data_receive, container,
				false);
		
		initializeNFC();
		
		return viewer;
	}
	
	/**
	 * initilazed the nfc adapter, and exits if not availiable
	 */
	private void initializeNFC() {
		Activity parent = getActivity();
        mNfcAdapter = NfcAdapter.getDefaultAdapter(parent);
        if (mNfcAdapter == null) {
            Toast.makeText(parent, "NFC is not available", Toast.LENGTH_LONG).show();
            parent.finish();
            return;
        }
	}

}
