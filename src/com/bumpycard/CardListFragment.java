package com.bumpycard;

import java.util.Map;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.bumpycard.entities.ContentWrapper;
import com.bumpycard.entities.Record;

/**
 * A list fragment representing a list of Cards. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link CardDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class CardListFragment extends Fragment {

	/**
	 * Tag used to filter logcat messages.
	 */
	private final String TAG = getClass().getName();
	
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * activated item position. Only used on tablets.
	 */
	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private Callbacks mCallbacks = sDummyCallbacks;

	/**
	 * The current activated item position. Only used on tablets.
	 */
	private int mActivatedPosition = ListView.INVALID_POSITION;

	/**
	 * The ExpandableListView that will hold the cards.  
	 */
	private ExpandableListView cardsELV;
	/**
	 * The ExpandableListAdapter for holding the cards.
	 */
	private ExpandableListAdapter cardsELAdapter;
	
	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		/**
		 * Callback for when an item has been selected.
		 */
		public void onItemSelected(Record record);
	}

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(Record record) {
		}
	};

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public CardListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
	    Log.i(TAG, "onCreateView");
	    
	    View viewer = (View) inflater.inflate(R.layout.fragment_card_list, container, false);
	    cardsELV = (ExpandableListView) viewer.findViewById(R.id.fragment_card_list_elv);
	    cardsELAdapter = new RecordExpandableListAdapter();
	    cardsELV.setAdapter(cardsELAdapter);
	    
	    cardsELV.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				
				mCallbacks.onItemSelected((Record) cardsELAdapter.getChild(groupPosition, childPosition));

				//uncomment if find way for two pane
//				long packedPosition = ExpandableListView.getPackedPositionForChild(groupPosition, childPosition);
//				int flatPosition = cardsELV.getFlatListPosition(packedPosition);
//				cardsELV.setItemChecked(flatPosition, true);

				return true;
			}
		});
		return viewer;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Log.i(TAG, "onViewCreated");
		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		Log.i(TAG, "onAttach");
		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		cardsELV.setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	/**
	 * Helper to set positions as activated.
	 * 
	 * @param flatPosition	the flat position to be marked activated
	 */
	private void setActivatedPosition(int flatPosition) {
		if (flatPosition == ListView.INVALID_POSITION) {
			cardsELV.setItemChecked(mActivatedPosition, false);
		} else {
			cardsELV.setItemChecked(flatPosition, true);
		}

		mActivatedPosition = flatPosition;
	}

	/**
	 * RecordExpandableListAdapter is the class that is responsible for mapping groups
	 * of tags and their corresponding adapters to an ExpandableListView.
	 * 
	 * @author brenthronk
	 *
	 */
	public class RecordExpandableListAdapter extends BaseExpandableListAdapter {
		
		/**
		 * Tag used for logcat.
		 */
		private String TAG = getClass().getName();
		
		/**
		 * ContentWrapper used to query database;
		 */
		private ContentWrapper mContentWrapper;
		
		/**
		 * Contains the tags for all the groups.
		 */
		private String[] groups;
		
		/**
		 * Records that match up to their corresponding group.
		 */
		private Record[][] children;
		
		public RecordExpandableListAdapter() {
			mContentWrapper = new ContentWrapper(CardListFragment.this.getActivity().getContentResolver());
			groups = mContentWrapper.getAllTags();
			children = new Record[groups.length][];
			for (int i = 0; i < groups.length; i++) {
				children[i] = mContentWrapper.getRecordsForTag(groups[i]);
			}
		}
		
		@Override
		public Object getChild(int groupPosition, int childPosition) {
			if (isChildPositionValid(groupPosition, childPosition))
				return children[groupPosition][childPosition];
			else
				return null;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                View convertView, ViewGroup parent) {
			
			if (!isChildPositionValid(groupPosition, childPosition))
				return null;
			
            TextView textView = getGenericView();
            Record childRecord = (Record) getChild(groupPosition, childPosition);
            textView.setText(childRecord.getName());
            
            //TODO change color if selected
//			long packedPosition = ExpandableListView.getPackedPositionForChild(groupPosition, childPosition);
//			int flatPosition = theELV.getFlatListPosition(packedPosition);
//            if (flatPosition == theELV.getCheckedItemPosition())
//            	textView.setBackgroundResource(R.color.child_color);
//            else
//            	textView.setBackgroundColor(getResources().getColor(R.color.translucent_red));
            
            return textView;
        }

		@Override
		public int getChildrenCount(int groupPosition) {
			if (!isGroupPositionValid(groupPosition))
				return 0;
			
			Record[] items = children[groupPosition];
			if (items != null)
				return items.length;
			else
				return 0;
		}

		@Override
		public Object getGroup(int groupPosition) {
			if (isGroupPositionValid(groupPosition))
				return groups[groupPosition];
			else
				return null;
		}

		@Override
		public int getGroupCount() {
			return groups.length;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                ViewGroup parent) {
			Log.d(TAG, "getgroupview position:" + groupPosition);
			if (!isGroupPositionValid(groupPosition))
				return null;
            TextView textView = getGenericView();
            textView.setText(getGroup(groupPosition).toString());
            return textView;
        }

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return isChildPositionValid(groupPosition, childPosition);
		}
		
		/**
		 * Helper method to return a generic row view.
		 * 
		 * @return	a generic view consisting of just a textview 
		 */
        public TextView getGenericView() {
            // Layout parameters for the ExpandableListView
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
            		ViewGroup.LayoutParams.MATCH_PARENT, 64);
            
            TextView textView = new TextView(getActivity());
            textView.setLayoutParams(lp);
            // Center the text vertically
            textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            // Set the text starting position
            textView.setPadding(36, 0, 0, 0);
            return textView;
        }
        
        /**
         * This is called to set the records and notify the adapter of the
         * change.
         * 
         * @param records	map of the tags, to an array of records for a given
         * 					tag
         */
		public void setRecords(Map<String, Record[]> records) {
			if (records == null)
				return;
			try {
				groups = records.keySet().toArray(new String[0]);
				children = new Record[groups.length][];
				for (int i = 0; i < groups.length; i++) {
					children[i] = records.get(groups[i]);
				}
				this.notifyDataSetChanged();
			} catch (Exception e) {
				Log.e(TAG, e.toString());
				groups = null;
				children = null;
			}
			
		}
		
		/**
		 * Simple helper method to check if groupPosition is in valid range.
		 * 
		 * @param groupPosition 	index into groups and first dimension of 
		 * 							children
		 * @return 					true if valid, false otherwise
		 */
		private boolean isGroupPositionValid(int groupPosition) {
			return (groups != null && 
					children != null && 
					groupPosition >= 0 &&
					groupPosition < groups.length &&
					groupPosition < children.length);
		}
		
		/**
		 * Simple helper method to check validity on child position.
		 * 
		 * @param groupPosition 	group of children index
		 * @param childPosition 	index of child within respective group
		 * @return 					true if valid, false otherwise
		 */
		private boolean isChildPositionValid(int groupPosition, int childPosition) {
			return (isGroupPositionValid(groupPosition) &&
					childPosition >= 0 &&
					childPosition < children[groupPosition].length);
		}
	}
}
