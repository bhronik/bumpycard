package com.bumpycard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class SignupFragment extends Fragment {

	private final String TAG = getClass().getName();

	private EditText mEmailEt;
	private EditText mPasswordEt;
	private EditText mPasswordConfirmEt;
	private Button mSubmitButton;
	
	public SignupFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View viewer =  inflater.inflate(R.layout.fragment_signup, container, false);
		
		initializeEditTexts(viewer);
		initialzeButton(viewer);
		
		return viewer;
	}

	/**
	 * @param viewer
	 */
	private void initializeEditTexts(View viewer) {
		mEmailEt = (EditText) viewer.findViewById(R.id.fragment_signup_email_et);
		mPasswordEt = (EditText) viewer.findViewById(R.id.fragment_signup_password_et);
		mPasswordConfirmEt = (EditText) viewer.findViewById(R.id.fragment_signup_password_confirm_et);
		
	}

	/**
	 * @param viewer
	 */
	private void initialzeButton(View viewer) {
		mSubmitButton = (Button) viewer.findViewById(R.id.fragment_signup_submit_button);
		mSubmitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submit();
			}
		});
	}

	private void submit() {
		// TODO sanitize email
		// TODO sanitize password
		//TODO submit to server
		
	}

}
